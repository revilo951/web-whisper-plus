package main

import (
	"flag"
	"net/http"
	"os"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	// Flags
	debug := flag.Bool("debug", false, "sets log level to debug")
	flag.Parse()

	// Logging
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// Load .env file
	log.Info().Msg("Loading .env file.")
	err := godotenv.Load()
	if err != nil {
		log.Info().Msg("No .env file found, using environment variables")
	}

	// Database
	log.Info().Msg("Initialising Database.")
	InitDB()
	Migrate()

	// Router
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	// cors
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300,
	}))

	r.Post("/asr", ProcessFrom)
	r.Get("/jobs", GetJobs)
	r.Get("/jobs/{id}", GetJobById)
	r.Get("/jobs/delete/{id}", DeleteJob)
	r.Post("/jobs/edit", EditJob)

	r.Get("/subtitles/{id}", GetSubs)

	r.Get("/file/{id}", GetFile)

	log.Info().Msg("Starting server on port 3000...")
	go MonitorJobs()
	err = http.ListenAndServe(":3000", r)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to start server")
	}
}
