package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
)

func ProcessFrom(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, "Invalid data", http.StatusBadRequest)
		return
	}

	file, header, err := r.FormFile("file")
	if err != nil {
		http.Error(w, "Invalid file", http.StatusBadRequest)
		return
	}
	defer file.Close()

	// Store file on disk
	// Get las job Id from database and increment
	var count int64
	DBCon.Unscoped().Model(&TranscriptionJob{}).Where("ID > ?", "0").Count(&count)

	newId := count + 1
	log.Printf("New job id: %v", newId)
	dst := fmt.Sprintf("%v/%v-%v", os.Getenv("UPLOAD_DIR"), newId, header.Filename)
	f, err := os.Create(dst)
	if err != nil {
		http.Error(w, "Error saving file", http.StatusInternalServerError)
		return
	}
	defer f.Close()

	if _, err := io.Copy(f, file); err != nil {
		http.Error(w, "Error saving file", http.StatusInternalServerError)
		return
	}

	job := TranscriptionJob{
		Status:   StatusPending,
		Method:   r.FormValue("method"),
		Task:     r.FormValue("task"),
		Language: r.FormValue("language"),
		Prompt:   r.FormValue("prompt"),
		Encode:   "true",
		Output:   "json",
		FileName: header.Filename,
	}

	if err := DBCon.Create(&job).Error; err != nil {
		http.Error(w, "Error saving job", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func GetJobs(w http.ResponseWriter, r *http.Request) {
	var jobs []TranscriptionJob
	// GEt all jobs from database with GORM with status done
	DBCon.Find(&jobs)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(jobs)
}

func GetJobById(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	log.Printf("Getting job %v", id)

	// Get job from database with GORM
	var job TranscriptionJob
	if err := DBCon.First(&job, id).Error; err != nil {
		http.Error(w, "Job not found", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(job)
}

func GetSubs(w http.ResponseWriter, r *http.Request) {
	var job TranscriptionJob
	id := chi.URLParam(r, "id")

	if err := DBCon.First(&job, id).Error; err != nil {
		http.Error(w, "Job not found", http.StatusNotFound)
		return
	}

	filePath := fmt.Sprintf("%v/%v-%v.srt", os.Getenv("UPLOAD_DIR"), job.ID, job.FileName)

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		filePath, err = JsonToSrt(&job)
		if err != nil {
			http.Error(w, "Error generating subtitles", http.StatusInternalServerError)
			return
		}
	}

	file, err := os.Open(filePath)
	if err != nil {
		http.Error(w, "Subtitles not found", http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%v.srt", job.FileName))
	w.Header().Set("Content-Type", "application/octet-stream")
	io.Copy(w, file)
}

func DeleteJob(w http.ResponseWriter, r *http.Request) {
	var job TranscriptionJob
	id := chi.URLParam(r, "id")
	log.Printf("Deleting job %v", id)
	if err := DBCon.First(&job, id).Error; err != nil {
		http.Error(w, "Job not found", http.StatusNotFound)
		return
	}

	// Remove job files from disk
	filePath := fmt.Sprintf("%v/%v-%v", os.Getenv("UPLOAD_DIR"), job.ID, job.FileName)
	if err := os.Remove(filePath); err != nil {
		fmt.Printf("ERROR: Error deleting file: %v", err)
	}

	if err := DBCon.Delete(&job).Error; err != nil {
		http.Error(w, "Error deleting job", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func GetFile(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	var job TranscriptionJob
	if err := DBCon.First(&job, id).Error; err != nil {
		http.Error(w, "Job not found", http.StatusNotFound)
		return
	}
	filePath := fmt.Sprintf("%v/%v-%v", os.Getenv("UPLOAD_DIR"), job.ID, job.FileName)

	// Serve the file
	http.ServeFile(w, r, filePath)
}

func EditJob(w http.ResponseWriter, r *http.Request) {
	// Declare an interface to store the JSON data
	var job map[string]interface{}
	// Decode the JSON job
	if err := json.NewDecoder(r.Body).Decode(&job); err != nil {
		http.Error(w, "Invalid job", http.StatusBadRequest)
		return
	}

	var jobModel TranscriptionJob
	jsonBytes, _ := json.Marshal(job)
	json.Unmarshal(jsonBytes, &jobModel)
	log.Printf("Updating job %v", jobModel.ID)

	// Save to database
	if err := DBCon.Save(&jobModel).Error; err != nil {
		http.Error(w, "Error updating job", http.StatusInternalServerError)
		return
	}
}
