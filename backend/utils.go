package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
)

type Segment struct {
	Start, End int
	Text       string
}

type Input struct {
	Lang string          `json:"language"`
	Se   [][]interface{} `json:"segments"`
	Txt  string          `json:"text"`
}

func JsonToSrt(j *TranscriptionJob) (string, error) {
	var i Input
	err := json.Unmarshal(j.ResultJson, &i)
	if err != nil {
		log.Printf("Error decoding response: %v", err)
		return "", err
	}

	o := fmt.Sprintf("%v/%v-%v.srt", os.Getenv("UPLOAD_DIR"), j.ID, j.FileName)
	f, err := os.Create(o)
	if err != nil {
		log.Printf("Error creating file: %v", err)
		return "", err
	}
	defer f.Close()

	for n, s := range i.Se {
		start := int(s[0].(float64))
		end := int(s[1].(float64))
		text := s[2].(string)
		fmt.Fprintf(f, "%d\n%s --> %s\n%s\n\n", n+1, formatTime(start), formatTime(end), strings.TrimSpace(text))
	}

	return o, nil
}

func formatTime(t int) string {
	s := t % 60
	m := (t / 60) % 60
	h := t / 3600
	return fmt.Sprintf("%02d:%02d:%02d,000", h, m, s)
}
