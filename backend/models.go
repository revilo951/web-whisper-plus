package main

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

const (
	StatusPending = 0
	StatusDone    = 1
	StatusError   = 2
)

type TranscriptionJob struct {
	gorm.Model
	Status     int            `json:"job_status"`
	Method     string         `json:"method"`
	Task       string         `json:"task"`
	Language   string         `json:"language"`
	Prompt     string         `json:"initial_prompt"`
	Encode     string         `json:"encode"`
	Output     string         `json:"output"`
	FileName   string         `json:"file_name"`
	ResultJson datatypes.JSON `json:"result"`
}
