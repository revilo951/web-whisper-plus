package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/joho/godotenv"
	"gorm.io/datatypes"
)

func MonitorJobs() {
	ASR_ENDPOINT := getASREndpoint()

	var jobs []TranscriptionJob
	for {
		DBCon.Where("status = ?", StatusPending).Find(&jobs)
		for _, job := range jobs {
			asrResponse, err := processJob(&job, ASR_ENDPOINT)
			if err == nil {
				updateJobSuccess(&job, asrResponse)
			} else {
				log.Printf("Error processing job: %v", err)
				updateJobError(&job)
			}
		}
		time.Sleep(3 * time.Second)
	}
}

func getASREndpoint() string {
	ASR_ENDPOINT := os.Getenv("ASR_ENDPOINT")
	if ASR_ENDPOINT == "" {
		err := godotenv.Load()
		if err != nil {
			panic(err)
		}
		ASR_ENDPOINT = os.Getenv("ASR_ENDPOINT")
	}
	return ASR_ENDPOINT
}

func processJob(job *TranscriptionJob, ASR_ENDPOINT string) (map[string]interface{}, error) {
	var asrResponse map[string]interface{}
	body, writer, err := prepareMultipartFormData(job)
	if err != nil {
		log.Printf("Error preparing multipart form data: %v", err)
		return asrResponse, err
	}

	url := fmt.Sprintf("http://%v/asr?method=%v&task=%v&language=%v&initial_prompt=%v&encode=%v&output=%v", ASR_ENDPOINT, job.Method, job.Task, job.Language, url.QueryEscape(job.Prompt), job.Encode, job.Output)
	log.Printf("Sending request to %v", url)
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		log.Printf("Error creating request: %v", err)
		return asrResponse, err
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Accept", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error sending request: %v", err)
		return asrResponse, err
	}
	defer resp.Body.Close()
	b, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		log.Printf("Invalid status: %v", resp.StatusCode)
		return asrResponse, errors.New("Invalid status")
	}

	log.Printf("Response: %v", string(b))
	if err := json.Unmarshal(b, &asrResponse); err != nil {
		log.Printf("Error decoding response: %v", err)
		return asrResponse, err
	}

	return asrResponse, nil
}

func prepareMultipartFormData(job *TranscriptionJob) (*bytes.Buffer, *multipart.Writer, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	part, err := writer.CreateFormFile("audio_file", job.FileName)
	if err != nil {
		log.Printf("Error creating form file: %v", err)
		return nil, nil, err
	}

	// Read file from disk
	filePath := fmt.Sprintf("%v/%v-%v", os.Getenv("UPLOAD_DIR"), job.ID, job.FileName)
	file, err := os.Open(filePath)
	if err != nil {
		log.Printf("Error opening file: %v", err)
		return nil, nil, err
	}
	defer file.Close()

	_, err = io.Copy(part, file)
	if err != nil {
		log.Printf("Error writing to form file: %v", err)
		return nil, nil, err
	}

	err = writer.Close()
	if err != nil {
		log.Printf("Error closing writer: %v", err)
		return nil, nil, err
	}

	return body, writer, nil
}

func updateJobSuccess(job *TranscriptionJob, asrResponse map[string]interface{}) {
	job.Status = StatusDone
	resJson, err := json.Marshal(asrResponse)
	if err != nil {
		log.Printf("Error encoding response json: %v", err)
		return
	}
	job.ResultJson = datatypes.JSON(resJson)
	err = DBCon.Save(job).Error
	if err != nil {
		log.Printf("Error saving job: %v", err)
	}
}

func updateJobError(job *TranscriptionJob) {
	job.Status = StatusError
	DBCon.Save(job)
}
