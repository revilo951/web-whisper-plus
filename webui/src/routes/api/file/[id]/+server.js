export async function GET({ params }) {
    let response = await fetch(`http://backend:3000/file/${params.id}`, {method: "GET"});
    return response;
}