import { json } from '@sveltejs/kit';

export async function POST({ request }) {
    // Get request body
    const body = await request.text();
    // Convert to JSON
    const jsonBody = JSON.parse(body);
	fetch('http://backend:3000/jobs/edit', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(jsonBody)});
    return json({ message: 'Job edited!' });
}
