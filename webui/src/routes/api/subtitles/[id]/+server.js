export async function GET({ params }) {
    let response = await fetch(`http://backend:3000/subtitles/${params.id}`, {method: "GET"});
    return response;
}