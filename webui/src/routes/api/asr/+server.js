import { json } from '@sveltejs/kit';

export async function POST({ request }) {
    const formData = await request.formData();
    fetch("http://backend:3000/asr", {method: "POST", body: formData});
    console.log(formData);
    return json({ message: 'Job created!' });
}
