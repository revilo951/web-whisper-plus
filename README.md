# Web Whisper+

## What is Web Whisper+?

Web Whisper+ is the rewritten version of the former web-whisper. It is a web suite for subtitle and transcription generation.

#### Demo video

<video autoplay controls src="https://farside.link/rimgo/zkePa3c.mp4"></video>

## Contents

- [Features](#features)
    - [Roadmap](#roadmap)
- [Self-hosting](#self-hosting)
    - [GPU support](#gpu-support)
    - [MacOS support](#macos-support)
- [Credits](#credits)

---

## Features

- Transcribe any video or audio file to text
    - Or record your audio directly from your microphone
- Generate subtitles for any video or audio file
    - Edit the generated subtitles with a simple and intuitive interface
    - Export the subtitles in SRT
- Job queue for batch processing files.
    - SQLite database to save transcription history and data.
- 100% Local
- Support for GPU (See self-hosting section)

### Roadmap

- [ ] Transcribe from URL (YouTube, Vimeo, etc.)
- [ ] Auto dubbing: generate a voice over from the generated subtitles in any language.
- [ ] Auto translation: translate the generated subtitles to any language.

---

## Self-hosting

> Please, report any issues you find while self-hosting the app by opening an [issues](https://codeberg.org/pluja/web-whisper-plus/issues/new).

Self hosting is pretty easy, just 3 steps:

1. Clone the repository: `git clone https://codeberg.org/pluja/web-whisper-plus.git`
2. Edit the `.env` file, select the model.
3. Start the containers: `docker compose up --build -d`

> Note: The first run may need a bit of waiting as the model needs to be downloaded.

Now you can visit http://localhost:8899 and use the app.

### GPU support

Follow the same steps as above, but in step 3 use this command:

`docker compose -f docker-compose.yml -f docker-compose.gpu.yml up --build -d`

> ! You may encounter issues, please report them by opening an [issue](https://codeberg.org/pluja/web-whisper-plus/issues/new).

### MacOS support

As the backend is based on [ahmetoner/whisper-asr-webservice](https://github.com/ahmetoner/whisper-asr-webservice), it will depend on it to run. I am unable to test this as I don't have a MacOS. From the official [ahmetoner/whisper-asr-webservice](https://github.com/ahmetoner/whisper-asr-webservice) readme:

> GPU passthrough does not work on macOS due to fundamental design limitations of Docker. Docker actually runs containers within a LinuxVM on macOS. If you wish to run GPU-accelerated containers, I'm afraid Linux is your only option.

> The :latest image tag provides both amd64 and arm64 architectures

## Credits

- [ahmetoner/whisper-asr-webservice](https://github.com/ahmetoner/whisper-asr-webservice) - The backend of the app is based on this project, so thanks to ahmetoner for his work.
- [SvelteKit](https://kit.svelte.dev/) - The frontend of the app is based on SvelteKit, so thanks to the SvelteKit team for their work.
    - [TailwindCSS](https://tailwindcss.com/) - TailwindCSS is used in the frontend for styling.
    - [DaisyUI](https://daisyui.com/) - DaisyUI is used in the frontend for styling along with TailwindCSS.
- [Golang](https://golang.org/) - The backend of the app is written in Go.
    - [Chi router](https://go-chi.io/#/) - The awesome Chi router is used in the backend.
    - [GORM](https://gorm.io/) - GORM is used to interact with the database.

---

## Disclaimer

I am working on this project on my free time and I am not a professional developer, so the code may not be the best. If you find any issues, please report them by opening an [issue](https://codeberg.org/pluja/web-whisper-plus/issues/new) or contribute by opening a [pull request](https://codeberg.org/pluja/web-whisper-plus/pulls).